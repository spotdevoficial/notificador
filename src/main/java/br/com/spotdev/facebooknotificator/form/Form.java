package br.com.spotdev.facebooknotificator.form;

import java.io.IOException;

import br.com.spotdev.facebooknotificator.facebook.Auth;
import br.com.spotdev.facebooknotificator.facebook.Servidor;
import facebook4j.FacebookException;
import insidefx.undecorator.Undecorator;
import insidefx.undecorator.UndecoratorScene;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Form {

	// Variáveis do objeto
	private FormController controller = null;	// Controller do form com eventos e etc..
	public static Auth AUTH = null;				// Variávei de autenticação com o facebook
	public static Servidor SERVIDOR = null;		// Variávei de conexão com o servidor
	
	// Constantes
	public static final String TUTORIAL_URL = "https://www.youtube.com/embed/I23N-XADEGA?showinfo=0";
	
	/**
	 * Tenta autenticar-se com um login salvo
	 */
	public Form(){
		
		try {
			AUTH = Auth.tryLoadSavedLogin();
		} catch (FacebookException e) {
			if(e.getErrorCode() == -1)
				FormUtils.precisaEstarConectadoAInternet();
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Cria o form e demostra para o usuário
	 */
	public void create(){
		Stage primaryStage = new Stage();
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/main.fxml"));
		FormController controller = new FormController();
		fxmlLoader.setController(controller);
		this.setController(controller);
        Region root = null;
		try {
			root = (Region) fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

        final UndecoratorScene undecoratorScene = new UndecoratorScene(primaryStage, root);

        primaryStage.setScene(undecoratorScene);
        primaryStage.sizeToScene();
        primaryStage.toFront();

        Undecorator undecorator = undecoratorScene.getUndecorator();
        primaryStage.setMinWidth(undecorator.getMinWidth());
        primaryStage.setMinHeight(undecorator.getMinHeight());
        
        primaryStage.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>()
        {
            @Override
            public void handle(WindowEvent window)
            {
                controller.onLoad(window);
            }
        });
        primaryStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>()
        {
            @Override
            public void handle(WindowEvent window)
            {
                System.exit(0);
            }
        });
        primaryStage.show();
	}
	
	public FormController getController() {
		return controller;
	}

	public void setController(FormController controller) {
		this.controller = controller;
	}
	
}
