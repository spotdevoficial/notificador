package br.com.spotdev.facebooknotificator.form;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class FormUtils {

	public static void avisar(String mensagem){
		Alert dialogoInfo = new Alert(Alert.AlertType.WARNING);
        dialogoInfo.setTitle("Lamento, tivemos um problema");
        dialogoInfo.setHeaderText("Algo deu errado ao tentar realizar esta ação.");
        dialogoInfo.setContentText(mensagem);
        dialogoInfo.showAndWait();
	}
	
	public static void info(String mensagem){
		Alert dialogoInfo = new Alert(Alert.AlertType.INFORMATION);
        dialogoInfo.setTitle("Informação");
        dialogoInfo.setHeaderText(null);
        dialogoInfo.setContentText(mensagem);
        dialogoInfo.showAndWait();
	}
	
	public static void precisaEstarConectadoAInternet(){
		Alert dialogoInfo = new Alert(Alert.AlertType.ERROR);
        dialogoInfo.setTitle("Lamento, tivemos um problema");
        dialogoInfo.setHeaderText("Você esta conectado a internet?");
        dialogoInfo.setContentText("Você precisa estar conectado a internet para "
        		+ "utilizar esta aplicação.\n\nA APLICAÇÃO SERÁ FECHADA.");
        Optional<ButtonType> result = dialogoInfo.showAndWait();
        if (result.get() == ButtonType.OK){
            System.exit(0);
        } 
	}
	
	public static void error(Exception ex){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Mensagem de erro");
		alert.setHeaderText("Ops, parece que houve um erro.");
		alert.setContentText("Envie a mensagem abaixo para contato@divulgadorfb.com.br para que"
				+ " possamos tentar ajudar:");

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		String exceptionText = sw.toString();

		Label label = new Label(ex.getMessage());

		TextArea textArea = new TextArea(exceptionText);
		textArea.setEditable(false);
		textArea.setWrapText(true);

		textArea.setMaxWidth(Double.MAX_VALUE);
		textArea.setMaxHeight(Double.MAX_VALUE);
		GridPane.setVgrow(textArea, Priority.ALWAYS);
		GridPane.setHgrow(textArea, Priority.ALWAYS);

		GridPane expContent = new GridPane();
		expContent.setMaxWidth(Double.MAX_VALUE);
		expContent.add(label, 0, 0);
		expContent.add(textArea, 0, 1);

		// Set expandable Exception into the dialog pane.
		alert.getDialogPane().setExpandableContent(expContent);

		alert.show();
	}
	
}
