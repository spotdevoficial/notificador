package br.com.spotdev.facebooknotificator.form;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.validator.routines.UrlValidator;

import br.com.spotdev.facebooknotificator.facebook.Auth;
import br.com.spotdev.facebooknotificator.facebook.Notificacao;
import br.com.spotdev.facebooknotificator.facebook.Servidor;
import br.com.spotdev.facebooknotificator.facebook.Usuario;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.RawAPIResponse;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class FormController {
	
	// Variáveis do objeto da classe
	private Usuario[] lastLoadedUsers = null;	// Usuários carregados para a exportação
	private boolean running = false;			// Variável que verifica se esta enviando notificação
	
	// Variáveis de possíveis buckups
	private List<Usuario> userBuckup = null;	// Lista de usuários que faltam
	private String messageBuckup = null;		// Mensagem da notificação
	private boolean hasBuckup = false;			// Se tem ou não buckup
	private Button buckupButton = null;			// Botão para reenviar
	
	/**
	 * Evento chamado quando a janela é carregada
	 * @param window Variável que representa o evento
	 */
	public void onLoad(WindowEvent window){
		
		WebEngine engine = webViewTutorial.getEngine();
		engine.load(Form.TUTORIAL_URL);
		
		// Cria um evento de quando o maineMenuPanel é definido como visivel
		mainMenuPanel.visibleProperty().addListener(new ChangeListener<Boolean>() {
	        @Override
	        public void changed(final ObservableValue<? extends Boolean> observableValue, final Boolean aBoolean, final Boolean visible) {
		        if(visible){
	        		// Tenta pegar o nome do usuário e do app logado para demostrar na interface do app
		        	try {
						autenticadoComoLabel.setText("Autenticado como: "+getAuth().getFacebook().getName());
						Facebook facebook = getAuth().getFacebook();
						RawAPIResponse response = facebook.callGetAPI(getAuth().getAppid());
						String nome = response.asJSONObject().getString("name");
						aplicativoLabel.setText("Aplicativo: "+nome);
						
						// Gera a URL para os leads se inscreverem
						generatedUrlField.setText(Form.SERVIDOR.getLoginUrl());
						
						// Verifica se tem buckup de alguma notificação
						if(checkByBuckup()){
							FormUtils.info("Parece que seu último envio não foi realizado com sucesso"
									+ "\n\nVocê pode facilmente tentar recomeça-lo de onde parou"
									+ "\nPressionando o botão 'Enviar notificação'");
						}
					} catch (FacebookException e) {
						if(e.getErrorCode() == -1)
							FormUtils.precisaEstarConectadoAInternet();
						else
							FormUtils.error(e);
					} catch (Exception e) {
						FormUtils.error(e);
						e.printStackTrace();
					}
		        }
	        }
	    });
		
		// Verifica se foi autenticado com sucesso
		if(Form.AUTH == null){	// Caso não, abre a tela de autenticação
			appSAppIdPanel.visibleProperty().set(true);
			blockableBottomPanel.setCenter(appSAppIdPanel);
		}else{					// Caso sim, verifica se já tem url de redirecionamento salvo
			Form.SERVIDOR = new Servidor(Form.AUTH, null);
			boolean success = Form.SERVIDOR.tryLoadSavedRedirectUrl();
			if(!success){		// Caso não, manda para a tela de configurar a url
				urlRedirectPanel.visibleProperty().set(true);
				blockableBottomPanel.setCenter(urlRedirectPanel);
			}else{				// Caso sim, vai para a tela principal
				tutorialPanel.visibleProperty().set(false);
				mainMenuPanel.visibleProperty().set(true);
			}
		}
		
	}
	
	/**
	 * Evento de quando o botão Prosseguir da tela de
	 * autenticação é clicado
	 */
	public void onPressProsseguir(){
		String appid = this.appid.getText();
		String apps = this.apps.getText();
		
		// Verifica se foi inserido os dados corretamentes
		if(appid == null || appid.isEmpty() || apps == null || apps.isEmpty()){
			FormUtils.avisar("Você deixou algum campo em branco, preencha todos"
					+ " por favor conforme o tutorial...");
			return;
		}
		
		// Tenta autenticar com os dados inseridos
		blockableBottomPanel.setDisable(true);
		Auth auth = new Auth(appid, apps);
		auth.setPermissions("email");
		auth.login((sucesso, mensagem, facebook)->{
			
			// Verifica se deu sucesso
			if(sucesso){	// Caso sim, vai para a tela de configurar a url
				Form.AUTH = auth;
				Form.SERVIDOR = new Servidor(Form.AUTH, null);
				
				urlRedirectPanel.visibleProperty().set(true);
				blockableBottomPanel.setCenter(urlRedirectPanel);
			}else{			// Caso não, tenta novamente
				if(!mensagem.equals("Janela fechada"))
					FormUtils.avisar(mensagem);
			}
			blockableBottomPanel.setDisable(false);
			
		});
	}
	
	/**
	 * Evento chamado quando o botão prosseguir da página de configurar
	 * a url de redirecionamento é aberto
	 */
	public void onPressProsseguirRedirectUrlButton(){
		
		String url = redirectUrlField.getText();
		
		// Verifica se a URl esta vazia
		if(url == null || url.isEmpty()){
			FormUtils.avisar("A URL inserida parece estar vazia.");
			return;
		}
		
		// Verifica se a url é válida
		UrlValidator urlValidator = new UrlValidator();
		if(!urlValidator.isValid(url)){
			FormUtils.avisar("A URL inserida parece ser inválida, insira a URL no seguinte formato:\n\n"
					+ "	Exemplo: http://www.suaurl.com.br");
			return;
		}
		
		// Salva a url e vai para a tela principal
		Form.SERVIDOR.setRedirectUrl(url);
		Form.SERVIDOR.saveRedrectUrl();
		urlRedirectPanel.visibleProperty().set(false);
		tutorialPanel.visibleProperty().set(false);
		mainMenuPanel.visibleProperty().set(true);
		
	}
	
	/**
	 * Evento chamado ao pressionar o botão de desconectar
	 * da tela principal
	 */
	public void onPressDesconectarButton(){
		Form.AUTH.logout();
		mainMenuPanel.visibleProperty().set(false);
		tutorialPanel.visibleProperty().set(true);
		blockableBottomPanel.setCenter(appSAppIdPanel);
		appSAppIdPanel.visibleProperty().set(true);
		FormUtils.info("Você foi desconectado com sucesso.");
	}
	
	/**
	 * Evento chamado ao pressionar o botão de salvar configuração
	 */
	public void onPressSalvarConfigButton(){
		String url = fieldNewRedirectUrl.getText();
		
		// Verifica se a URl esta vazia
		if(url == null || url.isEmpty()){
			FormUtils.avisar("A URL inserida parece estar vazia.");
			return;
		}
		
		// Verifica se a url é válida
		UrlValidator urlValidator = new UrlValidator();
		if(!urlValidator.isValid(url)){
			FormUtils.avisar("A URL inserida parece ser inválida, insira a URL no seguinte formato:\n\n"
					+ "	Exemplo: http://www.suaurl.com.br");
			return;
		}
		// Salva a url e vai para a tela principal
		Form.SERVIDOR.setRedirectUrl(url);
		Form.SERVIDOR.saveRedrectUrl();
		Stage stage = (Stage) fieldNewRedirectUrl.getScene().getWindow();
		stage.close();
		configButton.setDisable(false);
	}
	
	/**
	 * Evento chamado ao pressionar o botão de
	 * abrir janela de configuração
	 */
	public void onPressConfigButton(){
		configButton.setDisable(true);
		Stage primaryStage = new Stage();
		primaryStage.setTitle("Configurações da aplicação");
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/config.fxml"));
		fxmlLoader.setController(this);
        Region root = null;
		try {
			root = (Region)fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Scene scene = new Scene(root, root.getPrefWidth(), root.getPrefHeight());
		primaryStage.setScene(scene);
        primaryStage.toFront();
        
        primaryStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>(){

			@Override
			public void handle(WindowEvent event) {
				configButton.setDisable(false);
			}
        	
        });
        primaryStage.show();
	}
	
	/**
	 * Evento cahamdo ao pressionar o botão de criar uma nova notificação
	 * da tela principal
	 */
	public void onPressEnviarNotificacao(){
		
		// Mostra a tela de criação de notificação
		enviarNotificacaoButton.setDisable(true);
		Stage primaryStage = new Stage();
		primaryStage.setTitle("Inicie o envio de uma notificação");
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/notificacao.fxml"));
		fxmlLoader.setController(this);
        Region root = null;
		try {
			root = (Region)fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Scene scene = new Scene(root, root.getPrefWidth(), root.getPrefHeight());
		primaryStage.setScene(scene);
        primaryStage.toFront();
        
        primaryStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>(){

			@Override
			public void handle(WindowEvent event) {
				enviarNotificacaoButton.setDisable(false);
			}
        	
        });
        messageTextarea.textProperty().addListener(new ChangeListener<Object>() {

			 public void changed(ObservableValue<?> observable, Object oldValue, Object newValue) {
			
			   onMessageChanged((String)newValue);
			
			 }

		});
        
        primaryStage.show();
        
        // Avisa que existe um buckup
        if(hasBuckup){
        	buckupButton = new Button("Reenviar de onde parou");
        	buckupButton.setOnAction((event)->onPressRecomecarEnvioButton(event));
        	buckupButton.setPrefWidth(Double.MAX_VALUE);
        	notifyPanel.getChildren().add(4, buckupButton);
        	messageTextarea.setText(messageBuckup);
        	
        	FormUtils.info("A aplicação foi encerrada durante um envio\n\n"
        			+ "Você pode recuperar o envio de onde parou facilmente.");
        }
	}
	
	/**
	 * Salva o buckup de um envio de uma notificação
	 * @param message Mensagem da notificação
	 * @param usuarios Conjunto de usuários
	 * @throws IOException Erro caso não seja possível salvar
	 */
	public void saveBuckup(String message, Usuario[] usuarios) throws IOException{
		
		String data = message.replaceAll("\n", " ");
		StringBuilder sb = new StringBuilder(data);
		for(Usuario user : usuarios){
			sb.append("\n"+user.getId()+"\u0000"+user.getNome());
		}
		
		String appdata = System.getenv("APPDATA");
		Path path = Paths.get(appdata+"/.notificacao_buckup_"+Form.AUTH.getAppid());
		if(!Files.exists(path))
			Files.createFile(path);
		Files.write(path, sb.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
		
	}
	
	/**
	 * Deleta o buckup que tiver do appid
	 */
	public void removeBuckup(){
		String appdata = System.getenv("APPDATA");
		Path path = Paths.get(appdata+"/.notificacao_buckup_"+Form.AUTH.getAppid());
		if(Files.exists(path)){
			try {
				Files.delete(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Verifica se existe algum buckup de alguma
	 * notificação que não terminou de ser enviada
	 */
	public boolean checkByBuckup(){
		// Verifica se existe algum buckup de
		// alguma notificação que não terminou 
		// ser enviada
		String appdata = System.getenv("APPDATA");
		Path path = Paths.get(appdata+"/.notificacao_buckup_"+Form.AUTH.getAppid());
		try {
			List<String> buckupLines = Files.readAllLines(path);
			userBuckup = new ArrayList<>();
			hasBuckup = true;
			messageBuckup = buckupLines.get(0);
			for(int i = 1; i < buckupLines.size(); i++){
				String[] userData = buckupLines.get(i).split("\u0000");
				String id = userData[0];
				String nome = userData[1];
				Usuario usuario = new Usuario();
				usuario.setId(id);
				usuario.setNome(nome);
				userBuckup.add(usuario);
			}
			
			return true;
		} catch (IOException e1) {
			System.out.println("Sem buckups.");
			return false;
		}
		
	}
	
	/**
	 * Evento chamado quando uma mensagem da tela de envio de
	 * noficiação é editada
	 * @param newMessage Novamensagem que ficou no lugar da antiga
	 */
	public void onMessageChanged(String newMessage){
		int quantidade = newMessage.length();
		quantidadeCaracteresLabel.setText(quantidade+"/180 caracteres inseridos");
		if(quantidade > 180){
			messageTextarea.setText(newMessage.substring(0,180));
		}
	}
	
	/**
	 * Evento chamado quando o evento de recomeçar um envio é 
	 * chamado
	 */
	public void onPressRecomecarEnvioButton(ActionEvent event){
		String message = hasBuckup ? messageBuckup : messageTextarea.getText();
		sendNotification(message, true, buckupButton);
		
	}
	
	/**
	 * Evento chamado quando o botão de enviar
	 * a notificação para os usuários é pressionado
	 */
	public void onPressEnviarNotificacaoButton(){
		String message = messageTextarea.getText();
		sendNotification(message, false, enviarButton);
			
	}
	
	public void sendNotification(String message, boolean usingBuckup, Button blockableButton){
		Facebook facebook = Form.AUTH.getFacebook();
		
		if(message == null || message.isEmpty()){
			FormUtils.avisar("Parece que você não preencheu nada no campo de mensagem.");
			return;
		}
		blockableButton.setDisable(true);
		enviarButton.setDisable(true);
		Notificacao notificacao = new Notificacao(facebook, "", message);
		infoLabel.setText("Adquirindo usuários...");
		progressBar.setProgress(-1);
		
		// Cria em uma thread para dar tempo da mensagem de cima trocar
		new Thread(()->{
			final Usuario[] usuarios;
			// Adquiri os usuários
			try {
				usuarios = usingBuckup ? userBuckup.toArray(new Usuario[userBuckup.size()]) : Form.SERVIDOR.getUsers();
				saveBuckup(message, usuarios);
			} catch(Exception e){
				
				// Caso não consiga pegar os usuarios, fecha toda
				// a janela de envio
				Platform.runLater(()->{
					FormUtils.error(e);
					e.printStackTrace();
					Stage stage = (Stage)infoLabel.getScene().getWindow();
					stage.close();
				});
				blockableButton.setDisable(false);
				enviarButton.setDisable(false);
				return;
			}
			List<Usuario> userList = new LinkedList<>(Arrays.asList(usuarios));
			
			// Inicia o envio fora da thread
			int quantidade = usuarios.length;
			Platform.runLater(()->progressBar.setProgress(0));
			Platform.runLater(()->quantidadeEnvioLabel.setText("0/"+quantidade));
			try {
				Thread.sleep(10000);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			// Variável para verificar se tem um erro,
			// pois caso tenha era ira bloquiar outra mensagem
			// de ser mostrada que não seja de erro
			boolean error = false;
			running = true;
			new Thread(()-> {
				int oldValue = 0;
				while(running){
					int lenght = userList.size();
					if(lenght != oldValue){
						Usuario[] newArray = userList.toArray(new Usuario[lenght]);
						try {
							saveBuckup(message, newArray);
						} catch (Exception e) {
							e.printStackTrace();
						}
						oldValue = lenght;
					}
					try {
						Thread.sleep(500);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
			}).start();
			
			for(int i = 0; i < quantidade; i++){
				final int v = i;
				// Bloqueia mensagem de ser mostrada
				if(error != true){
					Platform.runLater(()->infoLabel.setText("Enviando para "+usuarios[v].getNome()));
				}
				try {
					notificacao.send(usuarios[i].getId());
					error = false;
					// Atualiza os status da contagem
					double porcentagem = (i+1)*100/quantidade;
					Platform.runLater(()->Platform.runLater(()->quantidadeEnvioLabel.setText((v+1)+"/"+quantidade)));
					progressBar.setProgress(porcentagem);
					userList.remove(usuarios[v]);
				} catch (Exception e) {
					// Trava o contador nesse usuario e manda mensagem de tentando reconectar
					i--;
					e.printStackTrace();
					Platform.runLater(()->infoLabel.setText("Tentando reconectar..."));
					error = true;
					try {
						Thread.sleep(3000);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
			running = false;
			removeBuckup();
			hasBuckup = false;
			blockableButton.setDisable(false);
			enviarButton.setDisable(false);
			if(buckupButton != null){
				Platform.runLater(()->notifyPanel.getChildren().remove(buckupButton));
			}
			Platform.runLater(()->infoLabel.setText("Envios encerrados."));
			
		}).start();
	}
	
	/**
	 * Evento chamado quando o botão de verificar
	 * as inscrições é pressionado
	 */
	@SuppressWarnings("unchecked")
	public void onPressInscricoes() {
		inscricoesButton.setDisable(true);
		Stage primaryStage = new Stage();
		primaryStage.setTitle("Usuarios inscritos para notificações");
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/usersLoaded.fxml"));
		fxmlLoader.setController(this);
        Region root = null;
		try {
			root = (Region)fxmlLoader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Scene scene = new Scene(root, root.getPrefWidth(), root.getPrefHeight());
		primaryStage.setScene(scene);
        primaryStage.toFront();
        
        Usuario[] usuarios = null;;
		try {
			usuarios = Form.SERVIDOR.getUsers();
		} catch (Exception e) {
			FormUtils.error(e);
			e.printStackTrace();
			primaryStage.close();
			return;
		}
        lastLoadedUsers = usuarios;
        ObservableList<Usuario> data = FXCollections.observableArrayList(Arrays.asList(usuarios));
        TableColumn<Usuario, String> nameCol = new TableColumn<Usuario, String>("Nome");
        TableColumn<Usuario, String> emailCol = new TableColumn<Usuario, String>("E-mail");
        
        nameCol.setCellValueFactory(
    	    new PropertyValueFactory<Usuario,String>("nome")
    	);
    	emailCol.setCellValueFactory(
    	    new PropertyValueFactory<Usuario,String>("email")
    	);
        
    	userTable.setItems(data);
        userTable.getColumns().addAll(nameCol, emailCol);
        primaryStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>(){

			@Override
			public void handle(WindowEvent event) {
				inscricoesButton.setDisable(false);
			}
        	
        });
        primaryStage.show();
	}
	
	/**
	 * Evento de quando o botão de Vídeo de como usar é pressionado
	 */
	public void onPressvideoComoUsar(){
		videoComoUsarButton.setDisable(true);
		Stage stage = new Stage();
		stage.setAlwaysOnTop(true);
		BorderPane pane = new BorderPane();
		stage.setScene(new Scene(pane, 560,315));
		
		WebView webView = new WebView();
		pane.setCenter(webView);
		WebEngine engine = webView.getEngine();
		stage.show();
		engine.load(Form.TUTORIAL_URL);
	
		stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				videoComoUsarButton.setDisable(false);
				engine.load("about:blank");
			}
		});
	}
	
	/**
	 * Evento de quando o botão de exportar é 
	 * pressionado
	 */
	public void onPressExportarButton(){
		
		StringBuilder builder = new StringBuilder();
		builder.append(lastLoadedUsers[0].getNome());
		builder.append("\t");
		builder.append(lastLoadedUsers[0].getEmail());
		for(int i = 1; i < lastLoadedUsers.length; i++){
			builder.append("\r\n");
			builder.append(lastLoadedUsers[i].getNome());
			builder.append("\t");
			builder.append(lastLoadedUsers[i].getEmail());
		}
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Arquivo de texto", "*.txt"));
		fileChooser.setTitle("Selecione o diretório em que deseja exportar seu arquivo");
		Stage stage = (Stage) exportarButton.getScene().getWindow();
		File file = fileChooser.showSaveDialog(stage);
		Path path = Paths.get(file.getAbsolutePath());
		if(path != null){
			try {
				Files.write(path, builder.toString().getBytes(), StandardOpenOption.CREATE);
				FormUtils.info("Seu arquivo foi exportado com sucesso.");
			} catch (IOException e) {
				FormUtils.error(e);
				e.printStackTrace();
			}
		}
	
	}
	
	public Auth getAuth() {
		return Form.AUTH;
	}

	@FXML public WebView webViewTutorial;
	@FXML public TextField appid;
	@FXML public TextField apps;
	@FXML public Button buttonProsseguir;
	@FXML public BorderPane blockableBottomPanel;
	@FXML public HBox appSAppIdPanel;
	@FXML public HBox urlRedirectPanel;
	@FXML public BorderPane mainMenuPanel;
	@FXML public BorderPane tutorialPanel;
	@FXML public AnchorPane mainPanel;
	@FXML public Button prosseguirRedirectUrlButton;
	@FXML public TextField redirectUrlField;
	@FXML public Label aplicativoLabel;
	@FXML public Label autenticadoComoLabel;
	@FXML public Button videoComoUsarButton;
	@FXML public TextField generatedUrlField;
	@FXML public Button inscricoesButton;
	@FXML public Button exportarButton;
	@FXML public TableView<Usuario> userTable;
	@FXML public TextField fieldNewRedirectUrl;
	@FXML public Button configButton;
	@FXML public Button enviarNotificacaoButton;
	@FXML public Text quantidadeCaracteresLabel;
	@FXML public TextArea messageTextarea;
	@FXML public Label infoLabel;
	@FXML public ProgressBar progressBar;
	@FXML public Label quantidadeEnvioLabel;
	@FXML public VBox notifyPanel;
	@FXML public Button enviarButton;
	
}
