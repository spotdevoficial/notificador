package br.com.spotdev.facebooknotificator.facebook;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.auth.AccessToken;
import javafx.application.Platform;
import javafx.concurrent.Worker.State;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * @description Classe para realizar uma autenticação com o facebook
 * @date 01/05/2016
 * @author Davi Salles
 */
public class Auth {

	// Variáveis utilizadas para a a autenticação com o app do facebook
	private String appid = null;			// APPID da aplicação
	private String appsecret = null;		// APPSECRET da aplicação
	private Facebook facebook = null; 		// Instancia do facebook que o objeto representa
	private String loginUrl = null;	 		// Url para realizar o login de outros usuários
	private String permissions = null;		// Permissões separadas por vírgula
	private AccessToken accessToken = null;	// AccessToken utilizado na autenticação
	
	
	/**
	 * @param appid AppId da aplicação criada no facebook
	 * @param appsecret	AppSecret da aplicação criada no facebook
	 */
	public Auth(String appid, String appsecret){
		this.appid = appid;
		this.appsecret = appsecret;
	}
	
	/**
	 * Função para realizar login no facebook
	 * @param event Evento chamado se o login der sucesso ou não
	 * @throws IOException Excption de caso esteja sem internet
	 */
	public void login(AuthEvent event){
		
		createFacebookInstance();
		
		try{
			// Realiza o login
			if(getAccessToken() != null){	// Caso tenha um login salvo
				facebook.setOAuthAccessToken(getAccessToken());
				facebook.getId();
				event.callEvent(true, "Login realizado com sucesso", facebook);
			} else {						// Caso não tenha, tenta logar com uma webview
				createLoginStage(facebook, event);
			}
		}catch(FacebookException e){
			switch(e.getErrorCode()){
			case -1:
				event.callEvent(false, "Você precisa se conectar a internet", facebook);
			default:
				e.printStackTrace();
				break;
			}
		}
		
	}
	
	/**
	 * Cria a instância do facebook para o objeto
	 */
	private Facebook createFacebookInstance(){
		Facebook facebook = new FacebookFactory().getInstance();
		this.facebook = facebook;
		facebook.setOAuthAppId(appid, appsecret);
		if(permissions != null)
			facebook.setOAuthPermissions(getPermissions());
		
		return facebook;
	}
	
	/**
	 * Cria o stage para realizar o login no facebook
	 * @param facebook Instancia do facebook que esta logando
	 * @param event Evento de caso o login seja realizado ou falhado
	 */
	private void createLoginStage(Facebook facebook, AuthEvent event){
		// Pega a URL para realizar o login
		String redirectUrl = "https://www.facebook.com/connect/login_success.html";
		loginUrl = facebook.getOAuthAuthorizationURL(redirectUrl)+"&display=popup";
		
		// Cria a janela para logar
		Platform.setImplicitExit(false);
		Stage stage = new Stage();
		BorderPane mainPane = new BorderPane();
		stage.setTitle("Realize o login para iniciar");
		stage.setScene(new Scene(mainPane,500,480));

		// Cria o webview na página de login
		WebView webView = new WebView();
		WebEngine engine = webView.getEngine();
		engine.load(loginUrl);
		mainPane.setCenter(webView);
		
		stage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent arg0) {
				
				event.callEvent(false, "Janela fechada", facebook);
				
			}
			
		});
		
		engine.getLoadWorker().stateProperty().
		addListener((ov, oldState, newState) -> {

			if(newState == State.FAILED){
				event.callEvent(false, "Você precisa se conectar a internet", facebook);
				stage.close();
			}
			
        	// Verifica a URL da tela
        	String url = engine.getLocation();
        	// Verifica se foi possível logar com sucesso
        	if (newState == State.SUCCEEDED && url.contains("code=")) {
				String accessCode = getAccessCode(url);
				
				if(accessCode.equals("200")){
					event.callEvent(false, "AccessToken retornou código 200", facebook);
				}
            	
            	// Faz o login na intancia
				try {
					AccessToken accessToken = facebook.getOAuthAccessToken(accessCode);
					facebook.setOAuthAccessToken(accessToken);
					saveAccessToken(accessToken);
                	
					// Fecha a janela e finaliza o login
                	stage.close();
                	event.callEvent(true, "Login realizado com sucesso", facebook);
				} catch (FacebookException e1) {
					e1.printStackTrace();
				}
        	
            }
            
        });
		stage.show();
	}
	
	/**
	 * Salvo o accessToken para realizar o login novamente depois
	 * @param accessToken AccessToken do login
	 */
	private void saveAccessToken(AccessToken accessToken){
		Path path = getAccessTokenPath();
		String a = accessToken.getToken();
		String text = a+"\u0000"+getAppid()+"\u0000"+getAppsecret();
		byte[] data = text.toString().getBytes();
		
		// Tenta salvar o login
		try {
			if(!Files.exists(path))
				Files.createFile(path);
			Files.write(path, data, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Função para verificar se existe um login salvo
	 * @return Retorna um auth já logado
	 */
	public static Auth tryLoadSavedLogin() throws FacebookException{
		Path accessFBPath = getAccessTokenPath();
		try {
		
			// Verifica se tem accessToken salvo
			String data = Files.readAllLines(accessFBPath).get(0);
			if(data != null){
				String[] dataArray = data.split("\u0000");
				String accessToken = dataArray[0];
				String appid = dataArray[1];
				String apps = dataArray[2];
				
				AccessToken accessTokenLoaded = new AccessToken(accessToken);
				Auth auth = new Auth(appid, apps);
				auth.setAccessToken(accessTokenLoaded);
				Facebook facebook = auth.createFacebookInstance();
				facebook.setOAuthAccessToken(accessTokenLoaded);
				
				// Verifica se esta logado
				try {
					facebook.getId();
					return auth;
				// Caso não esteja logado, chama um excpetion
				} catch (FacebookException e) {
					if(e.getErrorCode() != -1)
						auth.logout();
					throw e;
				}
			}else {
				return null;
			}
		} catch (IOException e) {
		}
		
		return null;
	}
	
	/**
	 * Faz a extração do AccessCode da url do facebook
	 * 
	 * @param url que sera extraida o accessToken
	 * @return Retorna o AccessCode da contado facebook
	 */
	private String getAccessCode(String url){
				
		int i = url.indexOf("code=")+5;
		url = url.substring(i);
		int endI = url.indexOf("&");
		if(endI >= 0)
			url = url.substring(0, endI);

		return url;
		
	}
	
	/**
	 * Retorna o diretório onde é salvo o AccessToken
	 * @return
	 */
	private static Path getAccessTokenPath(){

		String appdata = System.getenv("APPDATA");
		Path path = Paths.get(appdata+"/.accessFB");
		return path;
		
	}
	
	/**
	 * Verifica se o usuário esta logado
	 * @return Retorna verdadeiro caso esteja logado
	 */
	public boolean isLoggedIn(){
		if(getFacebook() != null){
			try {
				if(facebook.getId() != null)
					return true;
			} catch (Exception e) {
				return false;
			}
		}
		
		return false;
	}
	
	/**
	 * Remove o AccessToken salvo
	 */
	public void logout(){
		Path path = getAccessTokenPath();
		this.facebook = null;
		this.setAccessToken(null);
		this.setAppid(null);
		this.setAppsecret(null);
		this.setPermissions(null);
		try {Files.delete(path);} catch (IOException e1) {}
	}

	public String getPermissions() {
		return permissions;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
		if(facebook != null)
			logout();
	}
	
	public AccessToken getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(AccessToken accessToken) {
		this.accessToken = accessToken;
	}

	public String getLoginUrl() {
		return loginUrl;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppsecret() {
		return appsecret;
	}

	public void setAppsecret(String appsecret) {
		this.appsecret = appsecret;
	}

	public Facebook getFacebook() {
		return facebook;
	}
	
	/**
	 * Interface para o evento de autenticação no webview
	 * @author Davi Salles
	 */
	public interface AuthEvent {
		
		public void callEvent(boolean success, String message, Facebook facebook);
		
	}
	
}
