package br.com.spotdev.facebooknotificator.facebook;

/**
 * @description Classe que representa o usuário adquirido
 * do servidor
 * @author Davi Salles
 * @date 04/05/2016
 */
public class Usuario {

	// Variáveis privadas
	private String nome;	// Nome do usuário
	private String email;	// Nome do email
	private String id;		// Id do usuário
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
