package br.com.spotdev.facebooknotificator.facebook;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.auth.AccessToken;
import facebook4j.internal.org.json.JSONException;
import facebook4j.internal.org.json.JSONObject;

/**
 * @description Classe que representa uma notificação a ser feita
 * para um usuário no facebook
 * @date 02/05/2016
 * @author Davi Salles
 */
public class Notificacao {

	// Variáveis para a notificação
	private String href = null;		 // URL de redirecionamento
	private String message = null;	 // Mensagem da notificação
	private Facebook facebook = null;// Instância do facebook autenticada
	
	/**
	 * Construtor do notificacao
	 * @param facebook Instancia do facebook com o appid logado
	 * @param href Url que a notificação redirecionará
	 * @param message Mensagem que contém na notificação
	 */
	public Notificacao(Facebook facebook, String href, String message){
		this.facebook = facebook;
		this.href = href;
		this.message = message;
	}
	
	/**
	 * Faz o envio para um usuario pelo id
	 * @param userid Id do usuario
	 */
	public void send(String userid) throws SendNotificationException, FacebookException, IOException {
		
		AccessToken appAccessToken = null;
		try {
			appAccessToken = facebook.getOAuthAppAccessToken();
		} catch (FacebookException exception) {
			throw exception;
		}
		
		String url = "https://graph.facebook.com/"+userid+"/notifications";
		HttpPost httpPost = new HttpPost(url);
		HttpClient httpClient = HttpClients.createDefault();
		
		ArrayList<BasicNameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("access_token", appAccessToken.getToken()));
		params.add(new BasicNameValuePair("href", ""));
		params.add(new BasicNameValuePair("template", message));
		
		String content = null;
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
			HttpResponse response = httpClient.execute(httpPost);
			content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
			JSONObject jsonobject = new JSONObject(content);
			
			// Verifica se envio foi realizado com sucesso ou se falhou
			if(jsonobject.has("error")){	// ERRO
				String message = jsonobject.getJSONObject("error").getString("message");
				throw new SendNotificationException(content, message);
			}else if(!jsonobject.has("success")){		// SUCESSO
				String message = "A notificação não pode ser enviada por falha desconhecida.";
				throw new SendNotificationException(content, message);
			}
			
		} catch (IOException e) {
			throw e;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Faz envio para usuários em massa por uma arraylist com os ids
	 * @param userids Lista com ids dos usuários
	 */
	public void sendToAll(Usuario[] usuarios){
		for(Usuario user : usuarios){
			try {
				String userid = user.getId();
				send(userid);
			} catch (SendNotificationException e) {
				e.printStackTrace();
			} catch (FacebookException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Facebook getFacebook() {
		return facebook;
	}

	public void setFacebook(Facebook facebook) {
		this.facebook = facebook;
	}

	/**
	 * @description Classe de exceção para caso falhe um envio de uma notificação
	 * @author Davi Salles
	 * @date 02/05/2016
	 */
	public class SendNotificationException extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1814494344257519074L;
		private String json = null;
		
		public SendNotificationException(String json, String message){
			super(message);
			this.json = json;
		}

		public String getJson() {
			return json;
		}
		
	}
	
}
