package br.com.spotdev.facebooknotificator.facebook;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import facebook4j.internal.org.json.JSONArray;
import facebook4j.internal.org.json.JSONException;
import facebook4j.internal.org.json.JSONObject;

/**
 * @description Classe para se comunicar com o servidor do app
 * @author Davi Salles
 * @date 04/05/2016
 */
public class Servidor {
	
	// Variávies constantes
	private static final String SERVER_URL = "http://notificador.spotdev.com.br/";

	// Variáveis privadas do servidor
	private Auth auth = null;			// Autenticação que será utilizada para query no servidor
	private String redirectUrl = null;	// Url em que o lead irá depois de logar no facebook
	
	public Servidor(Auth auth, String redirectUrl){
		this.auth = auth;
		this.redirectUrl = redirectUrl;
	}
	
	/**
	 * Retorna uma lista de usuários a partir de um appid
	 * @return ArrauList de usuários
	 * @throws ClientProtocolException Exception no servidor
	 * @throws IOException Excpetion caso esteja sem internet
	 */
	public Usuario[] getUsers() throws ClientProtocolException, IOException{
		
		ArrayList<Usuario> usuarios = new ArrayList<>();
		HttpClient httpClient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(getUsersAPIUrl());
		HttpResponse response = httpClient.execute(httpGet);
		String responsejson = EntityUtils.toString(response.getEntity());
		
		try {
			JSONArray json = new JSONArray(responsejson);
			for(int i = 0; i < json.length(); i++){
				JSONObject userJson = json.getJSONObject(i);
				Usuario usuario = new Usuario();
				usuario.setNome(userJson.getString("nome"));
				usuario.setId(userJson.getString("user_id"));
				usuario.setEmail(userJson.getString("email"));
				usuarios.add(usuario);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return usuarios.toArray(new Usuario[usuarios.size()]);
	}
	
	/**
	 * Retorna a URL da api que pega os usuários
	 * cadastrados no appid
	 * @return String Url da api
	 */
	private String getUsersAPIUrl(){
		
		String url = SERVER_URL+"get.php";
		String appid = "?appid="+getAuth().getAppid();
		
		return url+appid;
		
	}
	
	/**
	 * Retorna o url para o lead executar o login
	 * @return String Retorna um URL para o lead
	 */
	public String getLoginUrl(){
		String url = SERVER_URL;
		String app = "app="+getAuth().getAppid();
		
		String appsecret = Base64.encodeBase64URLSafeString(getAuth().getAppsecret().getBytes());
		String s = "&s+"+appsecret;
		
		String redirect = null;
		try {
			redirect = "&redirect="+URLEncoder.encode(getRedirectUrl(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		String b = Base64.encodeBase64URLSafeString((app+s+redirect).getBytes());
		url+="?b="+b;
		
		url = getAuth().getFacebook().getOAuthAuthorizationURL(url);
		return url;
	}
	
	public void saveRedrectUrl(){
		try {
			if(!Files.exists(getSavePath()))
				Files.createFile(getSavePath());
			Files.write(getSavePath(), getRedirectUrl().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private Path getSavePath(){
		String appdata = System.getenv("APPDATA");
		Path path = Paths.get(appdata+"/."+getAuth().getAppid());
		return path;
	}
	
	public boolean tryLoadSavedRedirectUrl(){
		Path redirectPath = getSavePath();
		try {		
			String data = Files.readAllLines(redirectPath).get(0);
			if(data != null){
				setRedirectUrl(data);
				return true;
			}else {
				return false;
			}
		} catch (IOException e) {
		}
		
		return false;
	}
	
	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public Auth getAuth() {
		return auth;
	}

	public void setAuth(Auth auth) {
		this.auth = auth;
	}
	
}
