package br.com.spotdev.facebooknotificator;

import br.com.spotdev.facebooknotificator.form.Form;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * @description Essa aplicação tem como objetivo replicar uma das
 * funções do Lead Lovers. Função essa que envia notificações pelo
 * facebook para todos os inscritos no app criado pelo usuário.
 * @date 01/05/2016
 * @author Davi Salles
 */

public class FacebookNotificador extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		Form form = new Form();
		form.create();
		
	}
	
	public static void main(String args[]){
		launch(args);
	}

	
}
